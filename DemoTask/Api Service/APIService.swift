//
//  APIService.swift
//  DemoTask
//
//  Created by IOS20 on 19/05/20.
//  Copyright © 2020 Deftsoft. All rights reserved.
//

import Foundation
import UIKit

class APIService{
    let baseURL = "http://saudicalendar.com/api/user/getEventDetail"
    static let shared = APIService()
    
    func getData(successCallback: @escaping (([String:Any]) -> ())){
        guard let url = URL(string: baseURL) else {return}
        let parameterDictionary = ["latitude" : 28.1245, "longitude" : 78.1245,"event_id":12,"User_id":"00"] as [String : Any]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return
        }
        request.httpBody = httpBody

        Indicator.shared.showIndicator()
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if let response = response{
                    print(response)
                }
                if let data = data{
                    do{
                        Indicator.shared.hideIndicator()
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        if let data = json as? [String:Any]{
                            successCallback(data)
                        }
                    }catch{
                        print(error)
                    }
                }
            }
        }.resume()
    }
}

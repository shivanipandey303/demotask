//
//  ViewController.swift
//  DemoTask
//
//  Created by IOS20 on 18/05/20.
//  Copyright © 2020 Deftsoft. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var entertainmentLabel: UILabel!
    @IBOutlet weak var tabsCollectionView: UICollectionView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var organiserButton: UIButton!
    @IBOutlet weak var organiserCollectionView: UICollectionView!
    
    @IBOutlet weak var sponsorCollectionView: UICollectionView!
    @IBOutlet weak var sponsorButton: UIButton!
    @IBOutlet weak var organiserheight: NSLayoutConstraint!
    @IBOutlet weak var sponsorHeight: NSLayoutConstraint!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topMainView: UIView!
    
    var tabs = ["Overview","Additional info","Contact","Comment"]
    var selectedIndex = 0
    var lastContentOffset: CGFloat = 0.0
    let maxHeaderHeight: CGFloat = 472.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        organiserButton.tag = 1
        organiserCollectionView.isHidden = false
        sponsorCollectionView.isHidden = true
        entertainmentLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        entertainmentLabel.numberOfLines = 0
        entertainmentLabel.lineBreakMode = .byCharWrapping
        // Do any additional setup after loading the view.
    }

//MARK:- Button's Action
    @IBAction func tapOrganiserButton(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.setImage(UIImage(named: "minus"), for: .normal)
            organiserheight.constant = 180
            organiserCollectionView.isHidden = false
        }else{
            organiserCollectionView.isHidden = true
            sender.tag = 0
            sender.setImage(UIImage(named: "plus"), for: .normal)
            organiserheight.constant = 65
        }
    }
    
    @IBAction func tapSponsorButton(_ sender: UIButton) {
        if sender.tag == 0 {
            sponsorCollectionView.isHidden = false
            sender.tag = 1
            sender.setImage(UIImage(named: "minus"), for: .normal)
            sponsorHeight.constant = 180
        }else{
            sponsorCollectionView.isHidden = true
            sender.tag = 0
            sender.setImage(UIImage(named: "plus"), for: .normal)
            sponsorHeight.constant = 65
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == tabsCollectionView{
            return tabs.count
        }else{
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == organiserCollectionView{
            let cell = organiserCollectionView.dequeueReusableCell(withReuseIdentifier: "ListCollectionViewCell", for: indexPath) as! ListCollectionViewCell
            cell.userIage.cornerRadius = cell.userIage.frame.size.width/2
            return cell
        }else if collectionView == sponsorCollectionView{
            let cell = sponsorCollectionView.dequeueReusableCell(withReuseIdentifier: "sponsorCollectionViewCell", for: indexPath) as! ListCollectionViewCell
            cell.sponsorImage.cornerRadius = cell.sponsorImage.frame.size.width/2
            return cell
        }else{
            let cell = tabsCollectionView.dequeueReusableCell(withReuseIdentifier: "tabsCollectionViewCell", for: indexPath) as! ListCollectionViewCell
            cell.tabsLabel.text = tabs[indexPath.item]
            if selectedIndex == indexPath.item{
                cell.tabsLabel.textColor = UIColor.black
                cell.bottomView.isHidden = false
            }else{
                cell.tabsLabel.textColor = UIColor.lightGray
                cell.bottomView.isHidden = true
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == tabsCollectionView{
            selectedIndex = indexPath.item
            tabsCollectionView.reloadData()
        }
    }
}

//get data
extension ViewController{
    func getData(){
        APIService.shared.getData { (dict) in
            print(dict)
        }
    }
}



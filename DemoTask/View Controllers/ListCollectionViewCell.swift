//
//  ListCollectionViewCell.swift
//  DemoTask
//
//  Created by IOS20 on 19/05/20.
//  Copyright © 2020 Deftsoft. All rights reserved.
//

import UIKit

class ListCollectionViewCell: UICollectionViewCell {
    //organiser
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userIage: UIImageView!
    
    //sponsor
    
    @IBOutlet weak var sponsorLabel: UILabel!
    @IBOutlet weak var sponsorImage: UIImageView!
    
    //tabs
    @IBOutlet weak var tabsLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
}

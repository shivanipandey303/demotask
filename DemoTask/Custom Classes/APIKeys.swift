//
//  APIKeys.swift
//  DemoTask
//
//  Created by IOS20 on 18/05/20.
//  Copyright © 2020 Deftsoft. All rights reserved.
//


import Foundation

class APIKeys{
  
    static let kdata = "data"
    static let kEventImage = "ev_image"
    static let kImage = "image"
    static let kEventOrganiser = "event_organizer"
    static let kOName = "o_name"
    static let kOImage = "o_logo"
    static let kSName = "s_name"
    static let kSLogo = "s_logo"
}
